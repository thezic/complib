module.exports = function(api) {
  api.cache(true)

  const typescript = [
    '@babel/typescript', {
    }
  ]

  const common = [
    '@babel/env',
    typescript
  ]

  const overrides = [
    { test: /\.ts$/, presets: [...common] },
    { test: /\.tsx$/, presets: [...common,'@babel/react'] },
  ]

  return {
    overrides,
  }
}
