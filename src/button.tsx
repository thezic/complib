import * as React from 'react'
import styled from 'styled-components'

interface IProps {
  children: React.ReactNode
  color: string
}

export const MyButton = styled.button<IProps>`
  background: ${props => props.color};
  padding: 5px;
`

// export const MyButton = (props: IProps) => (<button style={{backgroundColor: props.color}}>{props.children}</button>)
// export const MyButton = () => (<span>My Component</span>)

export const myUtility = (name: string) => name.toUpperCase()

export function anotherUtility(name: string) {
  return name
}
